TEX

Converts mathematical formulae given in LaTeX code to the corresponding MathML
 representation using LaTeXML.
The TeX code can be entered in the form <math>\LaTeX</math>
No server setup is required.
